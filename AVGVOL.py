#@begin_imports
from __future__ import print_function
import sys
from pyspark import SparkContext

#@end_imports
#@begin_methods


#@end_methods
#@begin_setup

if __name__ == "__main__":



#@end_setup
#@begin_spark_prog

    sc = SparkContext(appName="AVGVOL")
    lines = sc.textFile(sys.argv[1])
    dateValueMap = lines.map(lambda (line):(line.split(",",2)[1],int(line.split(",",7)[6])))
    mapOnes = dateValueMap.map(lambda (date,vol): (date, (vol,1)))
    reduceRDD = mapOnes.reduceByKey(lambda (vol, count), (vol2, count2): (vol+vol2, count+count2))
    avgResult = reduceRDD.map(lambda (date, (sumVol, sumCount)):(date, sumVol/sumCount))

#@end_spark_prog

    #for x in avgResult.collect():
        #print(str(x))
    print(str(avgResult.count()))