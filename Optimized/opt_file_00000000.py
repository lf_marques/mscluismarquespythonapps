from __future__ import print_function
import sys
from pyspark import SparkContext


if __name__ == "__main__":


	sc = SparkContext()

	lines = sc.textFile(sys.argv[1])
	dateValueMap = lines.map(lambda (line):(line.split(",",2)[1],int(line.split(",",7)[6])))
	mapOnes = dateValueMap.map(lambda (date,vol): (date, (vol,1)))
	reduceRDD = mapOnes.reduceByKey(lambda (vol, count), (vol2, count2): (vol+vol2, count+count2)).cache()

	avgResult = reduceRDD.map(lambda (date, (sumVol, sumCount)):(date, sumVol/sumCount))
	#for x in avgResult.collect():
	#print(str(x))
	with open('AVGVOL.py.output.txt', 'a') as f:
		f.write(str(str(avgResult.count()))+"\n")

	maxResult = reduceRDD.max(lambda (date, (sumVol, sumCount)):sumVol)
	with open('MAXDAY.py.output.txt', 'a') as f:
		f.write(str(str(maxResult))+"\n")
